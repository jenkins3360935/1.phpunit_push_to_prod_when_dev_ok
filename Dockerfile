FROM jenkins/agent:alpine-jdk11

ENV PHP_VERSION=7.4

RUN mkdir /home/jenkins/src
COPY composer.json /home/jenkins

USER root

RUN echo "https://dl-cdn.alpinelinux.org/alpine/v3.13/community" >> /etc/apk/repositories
#RUN echo "https://dl-cdn.alpinelinux.org/alpine/v3.13/main" >> /etc/apk/repositories

RUN apk update   
RUN apk add --update --no-cache \
    php7-xml \
    php7-openssl \
    php7-phar \
    php7-tokenizer \
    php7-dom \
    php7-json \
    php7-mbstring \
    php7-xmlwriter 

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install

# Add user Jenkins access to modify this folder
RUN chown -R jenkins:jenkins /home/jenkins/vendor/bin

USER jenkins

