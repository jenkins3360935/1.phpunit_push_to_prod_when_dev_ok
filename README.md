# PHPunit_push_to_master_when_dev_ok<br>
https://hub.docker.com/r/jenkins/jenkins<br>
https://github.com/jenkinsci/docker/blob/master/README.md<br>
<br>
Example how CI/CD can work with phpunit test's.<br>
When we will push new code to remote branch dev, Jenkins will check every 2 minutes for<br>
changes in remote branch and if tests will pass it will merge code to remote branch master.<br>
All based only on docker container.
<br>
Whats included:<br>
Dockerfile with jenkins/agent:alpine-jdk11, php7.4 necessary to run composer and phpunit.<br>
Let's assume that this repository is/will be your project, so you have to move included dokcerfile<br>
to some other directory. This docker file is for you to bild your own image neccesary to do tests.<br>
You need to create dockerhub repository, build image form dockerfile and push it to it :<br>
docker build -t <repository_name> .<br>
docker tag <repository_name> <dockerhub_user_name>/<repository_name><br>
docker push <dockerhub_user_name>/<repository_name><br>
<br>
<br>

Setup jenkins new pipeline:<br>
Checkout minimum setting image: pipeline_minimum_settings.jpg<br>
<br>
<br>

Credentials:<br>
If you don't have any you need to configure. In your new pipeline go to Configure.<br>
Find Credentials, Add new -> SSH Username with private key. "Check Enter directly" and click 'add'.<br>
Then in terminal: "cat ~/.ssh/id_rsa" and copy/paste that to textbox.<br>
If you have Passphase dont forget to type it below!, add description too.<br>
Again in pipeline add in Configure your new credentials from select.<br>
<br>
<br>

Changes in Jenkinsfile:<br>
Change pipeline->environment to yours<br>
Change pipeline->agent->dockerContainer->image to full name of dockerhub repository you pushed your image to.<br>
Change every credentialsId variable to yours that you have setup in Jenkins credentials.<br>
<br>
<br>

Extra file- Jenkinsfile.template - good jenkins ci/cd template
<br>
<br>
Other additional extra stuff:<br>
You can install blueocean in plugins section<br>
If you would like to setup docker as cloud template (Manage Jenkins->Clouds) you will need "Docker Host URI"<br>
and the best way to get it is to install this container and inspect it: https://hub.docker.com/r/alpine/socat <br>
It will give you ip address and port nr.<br>
Checkout minimum setting of docker cloud details and docker agent templates": docker_cloud_details_and_agent_templates.jpg<br>
Basic very good youtube video: https://www.youtube.com/watch?v=6YZvp2GwT0A